<?php if ( 'on' == et_get_option( 'divi_back_to_top', 'false' ) ) : ?>

	<span class="et_pb_scroll_top et-pb-icon"></span>

<?php endif;

if ( ! is_page_template( 'page-template-blank.php' ) ) : ?>

			<footer id="main-footer">

				<?php // Pre Footer widget area
					if ( is_active_sidebar( 'pre_footer' ) ) : ?>
						<div id="pre-footer" class="widget-area" role="complementary">
							<div class="container clearfix">
								<?php dynamic_sidebar( 'pre_footer' ); ?>
							</div>
						</div>
				<?php endif; ?>

				<?php get_sidebar( 'footer' ); ?>


				<?php
				if ( has_nav_menu( 'footer-menu' ) ) : ?>

					<div id="et-footer-nav">
						<div class="container">
							<?php
								wp_nav_menu( array(
									'theme_location' => 'footer-menu',
									'depth'          => '1',
									'menu_class'     => 'bottom-nav',
									'container'      => '',
									'fallback_cb'    => '',
								) );
							?>
						</div>
					</div> <!-- #et-footer-nav -->

				<?php endif; ?>

				<div id="footer-bottom">
					<div class="container clearfix">
						<?php
							if ( false !== et_get_option( 'show_footer_social_icons', true ) ) {
								get_template_part( 'includes/social_icons', 'footer' );
							}
						?>

						<?php // new widget area for monarch icons
							if ( is_active_sidebar( 'footer_social_icons' ) ) : ?>
								<div id="footer-social-icons" class="et-social-icons widget-area" role="complementary">
								<?php dynamic_sidebar( 'footer_social_icons' ); ?>
								</div><!-- #primary-sidebar -->
						<?php endif; ?>


						<?php					
							// dont use Divi Footer Credits
							// echo et_get_footer_credits();

							// user HWP footer copyright instead
							echo hwp_get_footer_copyright();

							// "Powered by" text - functions.php
							/*
							if ( is_customize_preview() ) { //dont show to non-admins on customiser
								if (current_user_can( 'administrator')) {
									echo hwp_powered_by_attribution();
								}
							} else { // show everywhere else
								echo hwp_powered_by_attribution();
							} 
							*/
						?>
					</div>	<!-- .container -->
					
				</div>
			</footer> <!-- #main-footer -->
		</div> <!-- #et-main-area -->

<?php endif; // ! is_page_template( 'page-template-blank.php' ) ?>

	</div> <!-- #page-container -->

	<?php wp_footer(); ?>
</body>
</html>