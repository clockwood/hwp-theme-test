<?php

// This file is for functions related directly to overrides for the DIVI theme

// Recommended way to include parent theme styles.
//  (Please see http://codex.wordpress.org/Child_Themes#How_to_Create_a_Child_Theme)
//  
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

    // enqueue child styles
    wp_enqueue_style('child-theme', get_stylesheet_directory_uri() .'/style.css', array('parent-theme'));

    // get custom css and js for this site
    wp_enqueue_style( 'custom-style', get_stylesheet_directory_uri() . '/hwp-inc/custom/custom.css' );
    wp_enqueue_script( 'custom-js', get_stylesheet_directory_uri() . '/hwp-inc/custom/custom.js', array(), '', true );
    //wp_enqueue_script( 'hwp-custom-js', get_stylesheet_directory_uri() . '/js/custom.js', array(), '', true );
}

// Include admin styles
add_action( 'admin_enqueue_scripts', 'admin_enqueue_styles' );
function admin_enqueue_styles() {
    wp_register_style( 'admin-style', get_stylesheet_directory_uri() . '/admin-style.css', false, '1.0.0' );
    wp_enqueue_style( 'admin-style');
    wp_enqueue_script( 'hwp-admin-js', get_stylesheet_directory_uri() . '/js/hwp-admin.js', array(), '', true );
}

// child theme setup area
add_action( 'after_setup_theme', 'hwp_setup_theme' );
function hwp_setup_theme() {
    // enforce default size of Wordpress image sizes
    update_option( 'thumbnail_size_w', 150 );
    update_option( 'thumbnail_size_h', 150 );
    //update_option( 'medium_large_size_w', 0 ); // from divi?
    //update_option( 'medium_large_size_h', 0 ); // from divi?
    update_option( 'medium_size_w', 800 ); //
    update_option( 'medium_size_h', 9999 );
    update_option( 'large_size_w', 1080 ); //
    update_option( 'large_size_h', 9999 );

    // add new smaller size
    add_image_size( 'hwp_small_size', 320 );

}

// make custom image sizes selectable from WordPress admin
add_filter( 'image_size_names_choose', 'hwp_custom_img_sizes' );
function hwp_custom_img_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'hwp_small_size' => __( 'Small' ),
    ) );
}

/**
 * Register our sidebars and widgetized areas.
 * Add new Social Icons widget area in the footer so Monarch social icons can be placed there
 */
add_action( 'widgets_init', 'hwp_widgets_init' );
function hwp_widgets_init() {
    register_sidebar( array(
        'name'          => 'Footer - Social Icons',
        'id'            => 'footer_social_icons',
        'before_widget' => '<section>',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="rounded">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name'          => 'Pre Footer',
        'id'            => 'pre_footer',
        'before_widget' => '<section>',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="rounded">',
        'after_title'   => '</h2>',
    ) );
}


/* Add extra body classes to fix DIVI issues */
add_filter('body_class','hwp_add_body_classes');
add_filter('admin_body_class','hwp_add_body_classes');
function hwp_add_body_classes( $classes ) {

    // sidebar border disabled (customizer) - only apply to non-admin page
    if ( ( et_get_option('sidebar_border_disable') === true ) && ( ! is_admin() ) ) {
        $classes[] = 'sidebar-border-disable';
    }

    /* if no widgets in sidebar, dont show the side bar */
    if ( ! is_active_sidebar('sidebar-1') ) {
        // add new class for styling
        $classes[] = 'no-right-sidebar';
    }

    return $classes;
}   
/* End Add extra body classes */




/**
 * Add Divi CPT Custom Module - CPT GRID LAYOUT
 * This lets you display content from other post types (like the Divi blog module does) by adding the post name slug
 * 
 */
function DS_Custom_Modules(){
 if(class_exists("ET_Builder_Module")){
    include("ds-custom-modules.php");
 }
}

function Prep_DS_Custom_Modules(){
    global $pagenow;

    $is_admin = is_admin();
    $action_hook = $is_admin ? 'wp_loaded' : 'wp';
    $required_admin_pages = array( 'edit.php', 'post.php', 'post-new.php', 'admin.php', 'customize.php', 'edit-tags.php', 'admin-ajax.php', 'export.php' ); // list of admin pages where we need to load builder files
    $specific_filter_pages = array( 'edit.php', 'admin.php', 'edit-tags.php' );
    $is_edit_library_page = 'edit.php' === $pagenow && isset( $_GET['post_type'] ) && 'et_pb_layout' === $_GET['post_type'];
    $is_role_editor_page = 'admin.php' === $pagenow && isset( $_GET['page'] ) && 'et_divi_role_editor' === $_GET['page'];
    $is_import_page = 'admin.php' === $pagenow && isset( $_GET['import'] ) && 'wordpress' === $_GET['import']; 
    $is_edit_layout_category_page = 'edit-tags.php' === $pagenow && isset( $_GET['taxonomy'] ) && 'layout_category' === $_GET['taxonomy'];

    if ( ! $is_admin || ( $is_admin && in_array( $pagenow, $required_admin_pages ) && ( ! in_array( $pagenow, $specific_filter_pages ) || $is_edit_library_page || $is_role_editor_page || $is_edit_layout_category_page || $is_import_page ) ) ) {
        add_action($action_hook, 'DS_Custom_Modules', 9789);
    }
}
Prep_DS_Custom_Modules();

/**
 * // end Divi Custom Module
 */




/**
 * Add additional functionality
 */

// WP Customizer functions
require get_stylesheet_directory() . '/hwp-inc/customizer.php';

// General WP things not related to the current theme
require get_stylesheet_directory() . '/hwp-inc/extras.php';

// HWP / turnkeyWP related functions
require get_stylesheet_directory() . '/hwp-inc/hwp.php';

// 3rd party plugin overrides
require get_stylesheet_directory() . '/hwp-inc/plugins.php';

// Special files for each site - not packaged or version controlled
require get_stylesheet_directory() . '/hwp-inc/custom/custom.php';
