<?php
/**
 * Custom functions for the Customizer.
 */


// Registers options with the Theme Customizer
add_action( 'customize_register', 'hwp_customize_register', 20 );
function hwp_customize_register( $wp_customize ) {

    /* Add New Custom Controls */
    // enable/disable sidebar border
    $wp_customize->add_setting( 'et_divi[sidebar_border_disable]', array(
        //'default'       => 'on',
        'type'          => 'option',
        'capability'    => 'edit_theme_options',
        'transport'     => 'postMessage',
        'sanitize_callback' => 'wp_validate_boolean',
    ) );
    $wp_customize->add_control( 'et_divi[sidebar_border_disable]', array(
        'label'     => esc_html__( 'Disable Sidebar Border' ),
        'section'   => 'et_divi_general_layout',
        'type'      => 'checkbox',
        'priority'  => 20
    ) );

    // Main header background image
    $wp_customize->add_setting('et_divi[main_header_background]', array(
        //'default'           => 'image.jpg',
        'capability'     => 'edit_theme_options',
        'type'           => 'option',
    ));
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize,
        'et_divi[main_header_background]', array(
        'label'     => esc_html__( 'Header Background Image' ),
        'section'   => 'et_divi_header_primary',
        'priority'  => 11
    ) ) );

    // admin option to disable "powered by" attribution
    $wp_customize->add_setting( 'et_divi[disable_powered_by]', array(
        //'default'       => 'on',
        'type'          => 'option',
        'capability'    => 'update_core', //only admin has this capability
        'transport'     => 'postMessage',
        'sanitize_callback' => 'wp_validate_boolean',
    ) );
    $wp_customize->add_control( 'et_divi[disable_powered_by]', array(
        'label'     => esc_html__( 'Disable "Powered by" attribution' ),
        'section'   => 'et_divi_bottom_bar',
        'type'      => 'checkbox',
        'priority'  => 20
    ) );




    // add Copyright text area in place of Footer Credits
    $wp_customize->add_setting( 'et_divi[custom_footer_copyright]', array(
        'default'           => '',
        'type'              => 'option',
        'capability'        => 'edit_theme_options',
        'transport'         => 'postMessage',
        'sanitize_callback' => 'et_sanitize_html_input_text',
    ) );

    $wp_customize->add_control( 'et_divi[custom_footer_copyright]', array(
        'type'     => 'textarea',
        'section'  => 'et_divi_bottom_bar',
        'settings' => 'et_divi[custom_footer_copyright]',
        'label'    => esc_html__( 'Edit Footer Copyright' ),
        //'description' => __( 'Add your copyright statement here.' ),
    ) );



    // add Pre Footer section to Customizer menu
    $wp_customize->add_section( 'pre_footer', array(
        'title'    => esc_html__( 'Pre-Footer Area' ),
        'panel' => 'et_divi_footer_panel', //the panel to add it to
        'priority' => 10
    ) );

    // choose sub-header bg color
    $wp_customize->add_setting( 'et_divi[pre_footer_bg_color]',
        array(
            'default'       => 'rgba(0,0,0,0)',
            'type'          => 'option',
            'capability'    => 'edit_theme_options',
            'transport'     => 'postMessage',
            'sanitize_callback' => 'et_sanitize_alpha_color',
        )
    );
    $wp_customize->add_control( new ET_Divi_Customize_Color_Alpha_Control( $wp_customize,
            'et_divi[pre_footer_bg_color]', array(
                'label'      => __( 'Pre Footer Background Color' ),
                'section'    => 'pre_footer',
                'settings'   => 'et_divi[pre_footer_bg_color]',
                'description' => __( '' ),
            )
        )
    );

    /* Edit Existing Controls */
    // Remove "Disable Footer Credits" and "Edit Footer Credits" Divi controls
    // replaced with custom text field
    $wp_customize->remove_control('et_divi[disable_custom_footer_credits]');
    $wp_customize->remove_control('et_divi[custom_footer_credits]');


} //hwp_customize_register

// Writes styles out the <head> element of the page based on the configuration options saved in the Theme Customizer.
add_action( 'wp_head', 'hwp_add_customizer_css' );
function hwp_add_customizer_css() { 
?>
<?php
    // define variables - assign Customizer values for use in <style>
    $accent_color = et_get_option( 'accent_color', '#f0f0f0' );
    $main_header_background = et_get_option( 'main_header_background' );
    $pre_footer_bg_color = et_get_option( 'pre_footer_bg_color', 'rgba(0,0,0,0)' );
    //echo '<pre>'.et_get_option( 'sub_header_background').'</pre>';
?>
    <style id="child-theme-customizer-css">
        <?php if ( $main_header_background ) { ?>
            #main-header {
                background-image: url( <?php echo $main_header_background; ?> );
                background-position: center center;
                background-repeat: no-repeat;
                background-size: cover;
            }
        <?php } ?>
        <?php if ( $pre_footer_bg_color ) { ?>
            #pre-footer {
                background-color: <?php echo $pre_footer_bg_color; ?>;
            }
        <?php } ?>

        <?php if ( $accent_color ) { // apply theme accent color to various things ?>
           .class { color: <?php echo esc_html( $accent_color ); ?>; }
           .woocommerce .woocommerce-tabs ul.tabs li.active { border-color: <?php echo esc_html( $accent_color ); ?> !important; }
        <?php } ?>



        /* ////////////////// Tablet And Below \\\\\\\\\\\\\\\\\\ */
        @media all and (max-width: 959px) {
        } /* end @media */


        /* ////////////////// Tablet Portrait And Below \\\\\\\\\\\\\\\\\\ */
        @media all and (min-width: 480px) and (max-width: 959px) {

        } /* end @media */

        /* ////////////////// Smartphone Portrait And Below \\\\\\\\\\\\\\\\\\ */
        @media all and (max-width: 479px) {

        } /* end @media */

    </style>
<?php
} // end hwp_add_customizer_css



// Registers the Theme Customizer Preview with WordPress.
add_action( 'customize_preview_init', 'hwp_customizer_live_preview' );
function hwp_customizer_live_preview() {
    //$theme_version = et_get_theme_version();
    wp_enqueue_script(
        'hwp-customizer',
        get_stylesheet_directory_uri() . '/js/hwp-customizer-preview.js',
        array( 'customize-preview' ),
        filemtime(get_stylesheet_directory() . '/js/hwp-customizer-preview.js'), //add version for cache-busting
        //'',
        true
    );
    wp_localize_script( 'hwp-customizer', 'et_main_customizer_data', array(
        'original_footer_credits' => 'test', //hwp_get_footer_credits(),
    ) );
} // end hwp_customizer_live_preview

// add custom Stylesheet and JS for Customizer use
add_action( 'customize_controls_enqueue_scripts', 'hwp_customize_preview_css' );
function hwp_customize_preview_css() {
    wp_enqueue_style( 'hwp-customizer-styles', get_stylesheet_directory_uri() . '/hwp-customizer-styles.css', array(), '' );
    wp_enqueue_script( 'hwp-customizer-custom-js', get_stylesheet_directory_uri() . '/js/hwp-customizer-custom.js', array( 'jquery' ), '', true );
}


