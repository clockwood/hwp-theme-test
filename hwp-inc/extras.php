<?php
/**
 * Custom functions that act independently of the theme templates
 */


/* disable "Post via email" functionality in Writing Settings */
add_filter('enable_post_by_email_configuration', '__return_false');

/* Hide WordPress version strings from scripts and styles
 * @return {string} $src
 * @filter script_loader_src
 * @filter style_loader_src
 */
add_filter( 'script_loader_src', 'hwp_extras_remove_wp_version_strings' );
add_filter( 'style_loader_src', 'hwp_extras_remove_wp_version_strings' );
function hwp_extras_remove_wp_version_strings( $src ) {
     global $wp_version;
     parse_str(parse_url($src, PHP_URL_QUERY), $query);
     if ( !empty($query['ver']) && $query['ver'] === $wp_version ) {
          $src = remove_query_arg('ver', $src);
     }
     return $src;
}


/* Hide WP version strings from generator meta tag */
add_filter('the_generator', 'hwp_extras_remove_version');
function hwp_extras_remove_version() {
	return '';
}


/* Add extra body classes */
add_filter('body_class','hwp_extras_add_body_classes');
add_filter('admin_body_class','hwp_extras_add_body_classes');
function hwp_extras_add_body_classes( $classes ) {

    // sidebar border disabled (customizer)
    $current_user = new WP_User(get_current_user_id());
    $user_role = array_shift($current_user->roles);
    if ($user_role) {
        if (is_admin()) { // if on admin pages
            $classes .= ' role-'. $user_role;
            if ( ! current_user_can( 'administrator') ) {
                $classes .= ' not-admin ';
            }
        } else {
            $classes[] = ' role-'. $user_role;
        } 
    } else {
        $classes[] = ' not-logged-in ';
    }

    return $classes;
}   
/* End Add extra body classes */


/* Make current menu item active on posts and Custom post types
 * Posts - http://www.billerickson.net/customize-which-menu-item-is-marked-active/
 * CPTs - https://gist.github.com/gerbenvandijk/5253921
 *
 * @param array $classes, current menu classes
 * @param object $item, current menu item
 * @param object $args, menu arguments
 */
add_action('nav_menu_css_class', 'hwp_extras_add_current_nav_class', 10, 3 );
function hwp_extras_add_current_nav_class($classes, $item, $args) {
    
    // Getting the current post details
    global $post;

    // only run on post pages, so it doesn't error
    if ( $post && $post->post_type == 'post' ) {
        // 'blog' section (i.e. default posts)
        if ( ( is_singular( 'post' ) || is_category() || is_tag() ) && 'Blog' == $item->title ) {
            $classes[] = 'current-menu-item';
        } else { // custom posts types
            // Getting the post type of the current post
            $current_post_type = get_post_type_object(get_post_type($post->ID));
            $current_post_type_slug = $current_post_type->rewrite['slug'];
            // Getting the URL of the menu item
            $menu_slug = strtolower(trim($item->url));

            // If the menu item URL contains the current post types slug add the current-menu-item class
            if (strpos($menu_slug,$current_post_type_slug) !== false) {
               $classes[] = 'current-menu-item';
            }
        }
    }
    
    // Return the corrected set of classes to be added to the menu item
    return $classes;
}

/* End Make current menu item active on posts and Custom post types */