<?php
/**
 * Custom functions related to hwp / turnkeyWP
 */

/**
 * Disable the auto generated email sent to the admin after a core update
 */
 apply_filters( 'auto_core_update_send_email', false, $type, $core_update, $result );

/* 
 * Copyright footer text. Called in footer.php
 * This is DIVI-specific (et_get_option).
 */
if ( ! function_exists( 'hwp_get_footer_copyright' ) ) :
function hwp_get_footer_copyright() {  

    $color = et_get_option( 'bottom_bar_text_color' );
    $size = et_get_option( 'bottom_bar_font_size' );

    $hwp_attribution = hwp_powered_by_attribution();

    $copyright_format = '<%3$s id="footer-copyright" style="color: '.$color.'; font-size:'.$size.'px;"><span class="copyright">%1$s</span> %2$s</%3$s>';
    $footer_copyright = et_get_option( 'custom_footer_copyright', '' );

    return et_get_safe_localization( sprintf( $copyright_format, $footer_copyright, $hwp_attribution, 'div' ) );
}
endif;


/* 
 * "Powered By" footer text. Used in Copyright text above.
 * Show "Powered by" text in footer, unless admin selects Disable option in Customizer > Footer > Bottom bar.
 * This is DIVI-specific (et_get_option).
 */
if ( ! function_exists( 'hwp_powered_by_attribution' ) ) :
function hwp_powered_by_attribution() {

    //$color = et_get_option( 'bottom_bar_text_color' );
    $disable_powered_by = et_get_option( 'disable_powered_by', false );
    $powered_by_format = '<%2$s id="powered-by" >%1$s</%2$s>';
    $powered_by_text = 'Powered by <a href="#" title="turnkeyWP - managed website solution">TurnkeyWP</a>.';

    /* output */

    // if Powered By is disabled, return nothing
    if ( $disable_powered_by ) {
        return '';        
    }

    return et_get_safe_localization( sprintf( $powered_by_format, $powered_by_text, 'span' ) );
    
}
endif;
/* End Powered By */


/* Login page customizations */
// Add stylesheet to login page
add_action( 'login_enqueue_scripts', 'hwp_login_stylesheet' );
function hwp_login_stylesheet() {
    wp_enqueue_style( 'custom-login', get_stylesheet_directory_uri() . '/style.css' );
    //wp_enqueue_script( 'custom-login', get_stylesheet_directory_uri() . '/style-login.js' );
}
// change logo destination url
add_filter( 'login_headerurl', 'hwp_login_logo_url' );
function hwp_login_logo_url() {
    $url = 'http://www.turnkeywp.io';
    return $url;
    //return home_url();
}
// logo title
add_filter( 'login_headertitle', 'hwp_login_logo_url_title' );
function hwp_login_logo_url_title() {
    return 'Login to your custom TurnkeyWP website';
}
// custom error message
add_filter( 'login_errors', 'hwp_login_error_message' );
function hwp_login_error_message() {
    return "<strong>Oops!</strong> We didn't recognize that username or password. Please try again.";
}
// custom header message
add_filter( 'login_message', 'hwp_login_heading' );
function hwp_login_heading( $message ) {
    if ( empty($message) ){
        return '<p class="login-message">Please log in to access your TurnkeyWP website!</p>';
    } else {
        return $message;
    }
}
// custom footer text
add_action( 'login_footer','hwp_login_footer' );
function hwp_login_footer() {
    echo '<div class="login-footer-message">
    <p>Need help? <a href="http://www.turnkeywp.io">Visit support</a>.<br /></p>
    <p>Not a member? Get your very own affordable <a href="http://www.turnkeywp.io">custom marketing website</a>.</p>
    </div>';
}

/* END Login page customizations */



