<?php
/**
  * WooCommerce overrides
  */

/* Remove dashboard meta boxes */
add_action( 'admin_init', 'hwp_woocommerce_remove_meta_boxes', 1000 ); // was admin_menu
function hwp_woocommerce_remove_meta_boxes() {
    remove_meta_box( 'woocommerce_dashboard_recent_reviews', 'dashboard', 'normal'); // recent reviews

}


/* Breadcrumbs - https://docs.woocommerce.com/document/customise-the-woocommerce-breadcrumb */
// Change WooCommerce breadcrumb delimiter
add_filter( 'woocommerce_breadcrumb_defaults', 'hwp_woo_change_breadcrumb_delimiter' );
function hwp_woo_change_breadcrumb_delimiter( $defaults ) {
  // Change the breadcrumb delimeter from '/' to '>'
  $defaults['delimiter'] = ' &gt; ';
  return $defaults;
}
// Change the breadcrumb home text to 'Shop'
add_filter( 'woocommerce_breadcrumb_defaults', 'hwp_change_breadcrumb_home_text' );
function hwp_change_breadcrumb_home_text( $defaults ) {
  $defaults['home'] = 'Shop';
  return $defaults;
}
// Change the breadcrumb 'Home' path to /shop
add_filter( 'woocommerce_breadcrumb_home_url', 'hwp_woo_custom_breadrumb_home_url' );
function hwp_woo_custom_breadrumb_home_url() {
    return '/shop';
}
// Remove tghe breadcrumb from the /shop page
add_action( 'wp_head', 'hwp_remove_wc_breadcrumbs' );
function hwp_remove_wc_breadcrumbs() {
  if ( is_shop() ) {
    remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
  }
}


// Remove product Title so we can move it above image (woocommerce functions)
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
// Add title back
add_action( 'woocommerce_before_single_product', 'woocommerce_template_single_title', 10 );

// Filter the checkout fields
add_filter( 'woocommerce_checkout_fields', 'hwp_woocommerce_checkout_fields' );
function hwp_woocommerce_checkout_fields( $fields ) {
    // remove the phone field
    // https://docs.woocommerce.com/document/tutorial-customising-checkout-fields-using-actions-and-filters/#section-2
    unset($fields['billing']['billing_phone']);

    // remove company field
    // unset($fields['billing']['billing_company']);

    // make the billing email field fill up the entire space
    $fields['billing']['billing_email']['class'] = array('form-row-wide');

    return $fields;
}








/**
  * create Product Reviews admin page and menu item
  */
add_action( 'admin_menu', 'hwp_woocommerce_add_reviews_page' );
function hwp_woocommerce_add_reviews_page() {
  // Add menu item under Tools (https://codex.wordpress.org/Administration_Menus)
  // Using Menu Editor Pro to move this menu item under WooCommerce menu
  add_options_page( 'Product Reviews', 'Product Reviews', 'manage_options', 'product-reviews', 'hwp_woocommerce_list_product_reviews' );
}

function hwp_woocommerce_list_product_reviews() {
  if ( !current_user_can( 'manage_options' ) )  {
    wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
  }

  $args = array( // args for get_comments()
    'number'      => 100, 
    'status'      => 'approve', 
    'post_status' => 'publish', 
    'post_type'   => 'product',

  );
  $comments = get_comments( $args );
  $cargs = array( // args for wp_list_comments()
    'callback'    => 'hwp_product_reviews_comment' // custom layout and styling
  );
 
 
  echo '<div class="wrap">';
  echo '<h1>Product Reviews</h1>';
    /* foreach($comments as $comment) :
      echo($comment->comment_author . '<br />' . $comment->comment_content);
    endforeach; */
    echo '<ul class="commentlist">';
    wp_list_comments($cargs, $comments);
    echo '</ul>';

  echo '</div>';
}

/* this handles formatting output of Product Reviews comments  */
function hwp_product_reviews_comment($comment, $args, $depth) {
  if ( 'div' === $args['style'] ) {
      $tag       = 'div';
      $add_below = 'comment';
  } else {
      $tag       = 'li';
      $add_below = 'div-comment';
  }
  ?>

  <<?php echo $tag ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
  <?php if ( 'div' != $args['style'] ) : ?>
      <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
  <?php endif; ?>

  <div class="comment-author vcard">
      <?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
      <?php printf( __( '<cite class="fn">%s</cite> <span class="says">says:</span>' ), get_comment_author_link() ); ?>
  </div>
  
  <?php if ( $comment->comment_approved == '0' ) : ?>
       <em class="comment-awaiting-moderation"><?php _e( 'Comment is awaiting moderation.' ); ?></em>
        <br />
  <?php endif; ?>

  <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>">
      <?php
      /* translators: 1: date, 2: time */
      printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link( __( '(Edit)' ), '  ', '' );
      ?>
      <?php $post = get_post( $comment->comment_post_ID, 'OBJECT'); ?>
      <?php $post_image = get_the_post_thumbnail( $post->ID, 'thumbnail' ); ?>
      <?php d( $post ); ?>
      <a href="<?php print get_permalink($post->post_ID); ?>">
        <?php print $post->post_title; ?>
        <?php print $post_image; ?>
      </a>
  </div>

  <?php comment_text(); ?>

  <div class="reply">
      <?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
  </div>
  <?php if ( 'div' != $args['style'] ) : ?>
  </div>
  <?php endif; ?>

  <?php
} 