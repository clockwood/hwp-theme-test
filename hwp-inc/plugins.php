<?php
/**
 * Custom functions for 3rd party plugin overrides - included in functions.php.
 */

/**
 * Get screen info
 */
add_action( 'current_screen', 'wpdocs_this_screen' ); 
function wpdocs_this_screen() {
    $currentScreen = get_current_screen();
    if( $currentScreen->id === "widgets" ) {
        // Run some code, only on the admin widgets page
    }
  d( $currentScreen ); // print to debugger
}
 

/**
 * WooCommerce
 * load WooCommerce overrides if it is enabled
 */
if ( class_exists( 'woocommerce' ) ) {
  require get_stylesheet_directory() . '/hwp-inc/woocommerce.php';
}


/** Remove unwanted 3rd party panels / meta boxes
  * White Label CMS handles removal of WP's dashboard widgets, this handles the rest
  * https://codex.wordpress.org/Function_Reference/remove_meta_box
  */
add_action( 'admin_init', 'hwp_remove_meta_boxes', 1000 ); // was admin_menu
function hwp_remove_meta_boxes() {
  // syntax:
  // remove_meta_box( $id, $page, $context);
  //if ( ! current_user_can( 'administrator' ) ) { // dont hide from admin
    remove_meta_box( 'backwpup_become_inpsyder', 'dashboard', 'normal'); // BackWPUp
    remove_meta_box( 'edd_dashboard_sales', 'dashboard', 'normal'); // EDD

    remove_meta_box( 'aioseop-list', 'aioseop_metaboxes', 'normal'); // All in One SEO - mailing list signup
    remove_meta_box( 'aioseop-about', 'aioseop_metaboxes', 'side'); // All in One SEO - about box
    remove_meta_box( 'aioseop-support', 'aioseop_metaboxes', 'side'); // All in One SEO - support box
  //}
}


/* Change Seriously Simple Podcasting archive URL
 * https://www.seriouslysimplepodcasting.com/documentation/modifying-podcast-urls/
 * this frees up /podcast for use as a custom page
 * NOTE: this also sets the CPT permalink path for this post type
 */
add_filter( 'ssp_archive_slug', 'ssp_modify_podcast_archive_slug' );
function ssp_modify_podcast_archive_slug ( $slug ) {
  return 'podcasts';
}

add_filter( 'ssp_feed_slug', 'ssp_modify_podcast_feed_slug' );
function ssp_modify_podcast_feed_slug ( $slug ) {
  return 'podcasts';
}


/**
  * Display 'slow upload' notice on Media Library page 
  */
add_action( 'admin_notices', 'hwp_custom_admin_notice' );
function hwp_custom_admin_notice() {
  global $pagenow;
  if ( is_admin() && ($pagenow == 'upload.php' || $pagenow == 'media-new.php') ): //only show on admin pages / upload.php page  

              if ( current_user_can( 'administrator') ): //don't display for admin role
                echo '<div class="update-nag"><strong>Note:</strong> It may take a few moments for new images to upload while we optimize them for the best appearance and performance.</div>';
              endif;

  endif;
}


/**
 * All in One SEO
 */

// remove dashboard widget (method given by plugin developer)
add_filter( 'aioseo_show_seo_news', '__return_false' );
