/**
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

( function( $ ) {
   
    $(document).ready( function(){
        
        // add body classes in Customizer in real time to simulate effect 
        wp.customize( 'et_divi[sidebar_border_disable]', function( value ) {
            value.bind( function( to ) {
                var $body = $('body');
                if ( to ) {
                    $body.addClass( 'sidebar-border-disable' );
                } else {
                    $body.removeClass( 'sidebar-border-disable' );
                }
            } );
        } );

        wp.customize( 'et_divi[main_header_background]', function( value ) {
            value.bind( function( to ) {
                $( '#main-header' ).css( 'background-image', 'url( ' + to + ')' );
            } );
        } );

        // add body classes in Customizer in real time to simulate effect
        // preview doesn't work if the disabled box is 'checked' on page load because the powered by HTML is not output to the page in that case.
        wp.customize( 'et_divi[disable_powered_by]', function( value ) {
            value.bind( function( to ) {
                var $footer = $('footer');

                if ( to ) { // true / checked
                    $footer.addClass( 'powered-by-disable' );
                } else {
                    $footer.removeClass( 'powered-by-disable' );
                }
            } );
        } );

        // pre footer bg color
        wp.customize( 'et_divi[pre_footer_bg_color]', function( value ) {
            value.bind( function( to ) {
                $( '#pre-footer' ).css( 'background-color', to );
            } );
        } );

        // footer copyright 
        /* var $copyright = $('copyright');
        if ( ! $copyright.length ) {
            $( '#footer-bottom .container' ).prepend( '<p class="copyright"></p>' );
            $copyright = $('copyright');
        }*/
        wp.customize( 'et_divi[custom_footer_copyright]', function( value ) {
            value.bind( function( to ) {
                var $copy = $('.copyright');
                $copy.html( to );
            } );
        } );


    }); // $(document).ready()

} )( jQuery );
